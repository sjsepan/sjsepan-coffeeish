# Coffeeish Color Theme - Change Log

## [0.1.5]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.1.4]

- update readme and screenshot

## [0.1.3]

- fix titlebar border, FG/BG in custom mode

## [0.1.2]

- and update changelog

## [0.1.1]

- fix manifest and pub WF

## [0.1.0]

- fix manifest repo links
- consistent badges
- retain v0.0.4 for those that prefer earlier style

## [0.0.4]

- make lst act sel BG transparent

## [0.0.3]

- fix theme name in theme json
- fix possible syntax error in theme json
- fix scrollbar, minimap slider transparencies:
light:
"minimapSlider.activeBackground": "#00000040",
"minimapSlider.background": "#00000020",
"minimapSlider.hoverBackground": "#00000060",
"scrollbarSlider.activeBackground": "#00000040",
"scrollbarSlider.background": "#00000020",
"scrollbarSlider.hoverBackground": "#00000060",
- fix completion list contrast:
editorSuggestWidget.selectedBackground lightened (slightly)
list.hoverBackground OK

## [0.0.2]

- fix statusBar.noFolder BG and pkg screenshot

## [0.0.1]

- Initial release
