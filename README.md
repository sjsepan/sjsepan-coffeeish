# Coffeeish Theme

Coffeeish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-coffeeish_code.png](./images/sjsepan-coffeeish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-coffeeish_codium.png](./images/sjsepan-coffeeish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-coffeeish_codedev.png](./images/sjsepan-coffeeish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-coffeeish_ads.png](./images/sjsepan-coffeeish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-coffeeish_theia.png](./images/sjsepan-coffeeish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-coffeeish_positron.png](./images/sjsepan-coffeeish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/13/2025
